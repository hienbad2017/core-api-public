package com.hbad.sampleapp;

import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.util.Log;
import com.hbad.core.viewmodel.CountryViewModel;

public class MainActivity extends LifecycleActivity {

  CountryViewModel countryViewModel;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    countryViewModel = ViewModelProviders.of(this).get(CountryViewModel.class);
    countryViewModel.getCountry().observe(this, response -> {
      Log.d("RESOURCE", response.toString());
    });
  }
}
