package com.hbad.core.api;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by BAD on 7/21/2017.
 */

public interface CountryService {

  //http://services.groupkt.com/country/get/all
  @GET("country/get/all") Observable<Response<String>> getCountry();

}
