package com.hbad.core.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import com.hbad.core.api.CountryService;
import com.hbad.core.model.ApiResponse;
import com.hbad.core.model.Resource;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by BAD on 7/21/2017.
 */

public class CountryRepository {

  private CountryService countryService;

  public CountryRepository(){
    countryService = createRetrofit();
  }

  public CountryRepository(CountryService countryService){
      this.countryService = countryService;
  }

  private CountryService createRetrofit() {
    return new Retrofit.Builder()
        .baseUrl("http://services.groupkt.com")
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(CountryService.class);
  }

  private <T> LiveData<ApiResponse<T>> convertObserableToLiveData(Observable<Response<T>> data){
    MutableLiveData<ApiResponse<T>> result = new MutableLiveData<>();
     data.subscribeOn(Schedulers.io())
         .observeOn(Schedulers.io())
         .subscribe(response -> {
           result.postValue(new ApiResponse<>(response));
         }, throwable -> {
           result.postValue(new ApiResponse<>(throwable));
         });
     return result;
  }

  public LiveData<Resource<String>> getCountry(){
    return new NetworkBoundResource<String, String>(){
      @NonNull @Override protected LiveData<ApiResponse<String>> createCall() {
        return convertObserableToLiveData(countryService.getCountry());
      }

      @Override protected String processResponse(ApiResponse<String> response) {
        return response.body;
      }
    }.asLiveData();
  }

}
