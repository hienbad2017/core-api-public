package com.hbad.core.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.hbad.core.model.ApiResponse;
import com.hbad.core.model.Resource;

/**
 * Created by BAD on 7/19/2017.
 */

public abstract class NetworkBoundResource<ResultType, RequestType> {

  private final MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();

  @MainThread
  public NetworkBoundResource(){
    makeNetworkBoundResource();
  }

  private void makeNetworkBoundResource(){
    result.setValue(Resource.loading(null)); // show loading.
    LiveData<ApiResponse<RequestType>> apiResponse = createCall();
    result.addSource(apiResponse, response -> {
      result.removeSource(apiResponse);
      if (response.isSuccessful()){
        result.setValue(Resource.success(processResponse(response)));
      }else{
        onFetchFailed();
        result.setValue(Resource.error(response.errorMessage, null));
      }
    });
  }

  protected void onFetchFailed() {}

  @NonNull
  @MainThread
  protected abstract LiveData<ApiResponse<RequestType>> createCall();

  @WorkerThread
  protected abstract ResultType processResponse(ApiResponse<RequestType> response);

  public LiveData<Resource<ResultType>> asLiveData() {
    return result;
  }

}

