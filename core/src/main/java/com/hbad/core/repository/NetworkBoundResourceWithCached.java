package com.hbad.core.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.hbad.core.model.ApiResponse;
import com.hbad.core.model.Resource;

/**
 * Created by BAD on 7/19/2017.
 */

public abstract class NetworkBoundResourceWithCached<ResultType, RequestType> {

  private final MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();

  @MainThread
  public NetworkBoundResourceWithCached(){
    makeNetworkBoundResourceFromCachedData();
  }

  private void makeNetworkBoundResourceFromCachedData(){
    result.setValue(Resource.loading(null)); // show loading.
    LiveData<ResultType> dbSource = loadFromDb(); // check data from db.
    result.addSource(dbSource, data -> {
      result.removeSource(dbSource);
      if (shouldFetch(data)){ // check cached data before load new data from network.
        fetchFromNetwork(dbSource);
      }else{
        // load cached data.
        result.addSource(dbSource, newData -> result.setValue(Resource.success(newData)));
      }
    });
  }

  private void fetchFromNetwork(final LiveData<ResultType> dbSource){
    LiveData<ApiResponse<RequestType>> apiResponse = createCall();
    // we re-attach dbSource as a new source, it will dispatch its latest value quickly
    result.addSource(dbSource, newData -> result.setValue(Resource.loading(newData)));
    result.addSource(apiResponse, response -> {
      result.removeSource(dbSource);
      result.removeSource(apiResponse);
      if (response.isSuccessful()){
        saveCallResult(processResponse(response));
        // we specially request a new live data,
        // otherwise we will get immediately last cached value,
        // which may not be updated with latest results received from network.
        // if you cached data from database, you can get data from db and set result to UI
        result.addSource(loadFromDb(), newData -> result.setValue(Resource.success(newData)));
      }else{
        onFetchFailed();
        result.addSource(dbSource, newData -> result.setValue(Resource.error(response.errorMessage, newData)));
      }
    });
  }

  protected void onFetchFailed() {}

  @MainThread
  protected abstract LiveData<ResultType> loadFromDb();

  @MainThread
  protected abstract boolean shouldFetch(@Nullable ResultType data);

  @NonNull
  @MainThread
  protected abstract LiveData<ApiResponse<RequestType>> createCall();

  @WorkerThread
  protected abstract void saveCallResult(@NonNull RequestType item);

  @WorkerThread
  protected RequestType processResponse(ApiResponse<RequestType> response) {
    return response.body;
  }

  public LiveData<Resource<ResultType>> asLiveData() {
    return result;
  }

}

