package com.hbad.core.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import com.hbad.core.model.Resource;
import com.hbad.core.repository.CountryRepository;

/**
 * Created by BAD on 7/21/2017.
 */

public class CountryViewModel extends ViewModel {

  private CountryRepository countryRepository;

  public CountryViewModel(){
    countryRepository = new CountryRepository();
  }

  public CountryViewModel(CountryRepository countryRepository){
    this.countryRepository = countryRepository;
  }

  public LiveData<Resource<String>> getCountry(){
    return countryRepository.getCountry();
  }

}
