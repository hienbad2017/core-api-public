package com.hbad.core.model;

import android.support.annotation.Nullable;
import java.io.IOException;
import retrofit2.Response;

/**
 * Created by BAD on 7/21/2017.
 * A generic class to get response from network by Retrofit.
 * @param <T>
 */

public class ApiResponse<T> {

  /**
   * return status code for this response.
   */
  public final int code;
  /**
   * return @body when get response from network successful
   */
  @Nullable
  public final T body;
  /**
   * return @errorMessage when get response from network failed. When isSuccessful() == false.
   */
  @Nullable
  public final String errorMessage;

  public ApiResponse(Throwable error) {
    code = 500;
    body = null;
    errorMessage = error.getMessage();
  }

  public ApiResponse(Response<T> response) {
    code = response.code();
    if (response.isSuccessful()) {
      body = response.body();
      errorMessage = null;
    } else {
      String message = null;
      if (response.errorBody() != null) {
        try {
          message = response.errorBody().string();
        } catch (IOException ignored) {
          //Timber.e(ignored, "error while parsing response");
        }
      }
      if (message == null || message.trim().length() == 0) {
        message = response.message();
      }
      errorMessage = message;
      body = null;
    }
  }

  public boolean isSuccessful() {
    return code >= 200 && code < 300;
  }

}
